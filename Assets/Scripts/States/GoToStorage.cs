using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//GoToStorage pu� essere chiamato quando l'IA si trova al centro ad osservare il progetto o quando si trova al tavolo da lavoro a valutare gli elementi mancanti
public class GoToStorage : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToStorage(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.GoToStorage;               //setto il nome dello stato attuale a GoToStorage
        agent.speed = 14;                       //setto la velocit� del robot
        agent.isStopped = false;                //e mi assicuro che l'IA possa muoversi
    }
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", true);        //attivo l'animazione di rotazione e di apertura e disattivo quello di camminata
        anim.SetBool("Walk_Anim", false);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }
    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        Box b = box.GetComponent<Box>();                                                            //creo una variabile di tipo Box che si riferisca alla scatola del primo elemento raccoglibile
        Box secondB = GameManager.instance.currentToolsPoints[0].GetComponent<Box>();               //e una variabile di tipo Box che si riferisca alla scatola del secondo elemento raccoglibile

        if (b.isEmpty && GameManager.instance.numOfMaterialsTaken == 0)                             //se la prima scatola � vuota e il robot non ha ancora preso il primo materiale
            StorageDestination(b);                                                                  //il robot va al magazzino relativo al materiale esaurito

        if (secondB.isEmpty && GameManager.instance.numOfMaterialsTaken != 0)                       //se la seconda scatola � vuota e il robot ha gi� preso il primo materiale
            StorageDestination(secondB);                                                            //il robot va al magazzino relativo al materiale esaurito

        //se il robot si trova in uno dei magazzini
        if (agent.transform.position == storage[0].position || agent.transform.position == storage[1].position || agent.transform.position == storage[2].position)
        {
            if (GameManager.instance.numOfMaterialsTaken == 0)                                      //se il robot non ha ancora preso il primo materiale
                Refill(b);                                                                          //riempie la scatola del primo materiale

            else
                Refill(secondB);                                                                    //senn� riempie la scatola del secondo materiale

            VisualEffects();                                                                        //attivo l'animazione sulla scatola appena riempita

            nextState = new GoToBox(agent, anim, box, storage, table, recharge, center, cBelt);     //passo allo stato successivo: andare all'altra scatola
            stage = Event.Exit;                                                                     //ed esco dallo stato attuale
            return;
        }

        base.Update();
    }
    public void VisualEffects()                                 //in base all'indice del magazzino in cui il robot entra, attivo il VFX della scatola appena riempita
    {
        if (agent.transform.position == storage[0].position)
            VFX.instance.boxRechargeVFX[0].SetActive(true);

        if (agent.transform.position == storage[1].position)
            VFX.instance.boxRechargeVFX[1].SetActive(true);

        if (agent.transform.position == storage[2].position)
            VFX.instance.boxRechargeVFX[2].SetActive(true);
    }
    public void StorageDestination(Box box)                     //in base al materiale contenuto precedentemente nella scatola vuota, il robot va verso il magazzino relativo allo stesso materiale
    {
        if (box.type == TypeOfTool.Gears)
            agent.SetDestination(storage[0].position);

        if (box.type == TypeOfTool.Wheels)
            agent.SetDestination(storage[1].position);

        if (box.type == TypeOfTool.Bolts)
            agent.SetDestination(storage[2].position);
    }
    public void Refill(Box box)                                 //la scatola si riempie e il conteggio di materiali torna al massimo della capienza
    {
        box.materialsInTheBox.SetActive(true);
        box.counter = box.maxTools;
    }
    public override void Exit()                                 //esco dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
