using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//il secondo stato � Watch, in cui il robot osserva l'ordine in arrivo e immagazzina le informazioni necessarie per proseguire
public class Watch : AIState
{
    public static event Action OnRandomChoice;      //creo un evento che gestisca la scelta randomica dell'oggetto da riparare
    public static event Action ChoiceOfBoxes;       //creo un evento che gestisca l'aggiunta delle due scatole di materiali alla lista di quelli da utilizzare per l'oggetto corrente


    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public Watch(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.Watch;                                             //setto il nome dello stato attuale a Watch
    }

    public float Timer;                                                 //creo un timer che permetta al robot di osservare gli schermi prima di riprendere la sua corsa
    
    public override void Enter()                                        //richiamo lo status Enter che funge da equivalente dello Start
    {
        OnRandomChoice += UIManager.Instance.RandomChoice;              //richiamo l'evento che sceglie randomicamente l'oggetto da riparare
        OnRandomChoice?.Invoke();
        ChoiceOfBoxes += GameManager.instance.AssignToolsToPoints;      //richiamo l'evento che aggiunge le scatole di materiali alla lista di quelli utili per il progetto attuale
        ChoiceOfBoxes?.Invoke();
        agent.isStopped = true;                                         //mi assicuro che il robot si fermi
        anim.SetBool("Roll_Anim", false);                               //disattivo le animazioni di movimento, ma lascio che il robot rimanga aperto
        anim.SetBool("Walk_Anim", false);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }

    public override void Update()                                       //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        Timer += Time.deltaTime;                                        //parte il timer di 4 secondi
        if (Timer > 4)
        {
            //se la scatola scelta non � vuota
            if (!box.GetComponent<Box>().isEmpty)
            {
                nextState = new GoToBox(agent, anim, box, storage, table, recharge, center, cBelt);     //passa allo stato successivo: andare alla scatola
                stage = Event.Exit;                                                                     //e chiude ufficialmente lo stato attuale
                return;
            }

            //se la scatola scelta � vuota
            if (box.GetComponent<Box>().isEmpty)
            {
                nextState = new GoToStorage(agent, anim, box, storage, table, recharge, center, cBelt); //passa allo stato successivo: andare al magazzino
                stage = Event.Exit;                                                                     //e chiude ufficialmente lo stato attuale
                return;
            }
        }
        base.Update();
    }
    public override void Exit()                                         //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
