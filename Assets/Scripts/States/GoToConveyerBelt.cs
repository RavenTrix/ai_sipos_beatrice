using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//GoToConveyerBelt � lo stato finale di consegna: quando il robot ha trascorso 4 secondi a riparare l'oggetto, porta il risultato sul nastro trasportatore
public class GoToConveyerBelt : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToConveyerBelt(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.GoToConveyerBelt;          //setto il nome dello stato attuale a GoToConveyerBelt
        agent.speed = 10;                       //setto la velocit� del robot
        agent.isStopped = false;                //e mi assicuro che l'IA possa muoversi
    }
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //disattivo l'animazione di rotazione e attivo quella di camminata e apertura
        anim.SetBool("Walk_Anim", true);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }
    public override void Update()                           //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        agent.SetDestination(cBelt.position);               //setto come destinazione del robot il nastro trasportatore
        if (agent.transform.position == cBelt.position)     //quando ci arriva
        {
            ActivateResultObj();                            //si attiva il risultato sul nastro trasportatore
            DeactivateUI();                                 //e si disattivano gli oggetti rappresentati sugli schermi e i loro contatori

            GameManager.instance.currentToolsPoints.Clear();                                                //resetto la lista di materiali che servivano per il progetto precedente
            nextState = new GoToCenter(agent, anim, box, storage, table, recharge, center, cBelt);          //passo allo stato successivo: tornare al centro
            stage = Event.Exit;                                                                             //ed esco dallo stato attuale
            return;
        }
        base.Update();
    }
    public void ActivateResultObj()                                     //in base al risultato rappresentato sugli schermi, si attiva l'oggetto giusto sul nastro trasportatore
    {
        if (UIManager.Instance.result[0].activeSelf)
            ToolsManager.Instance.bigWheelBarrow.SetActive(true);
        if (UIManager.Instance.result[1].activeSelf)
            ToolsManager.Instance.bike.SetActive(true);
        if (UIManager.Instance.result[2].activeSelf)
            ToolsManager.Instance.smallWheelBarrow.SetActive(true);
    }
    public void DeactivateUI()                                          //tutti gli oggetti rappresentati sugli schermi si disattivano e anche i loro contatori
    {
        foreach (GameObject res in UIManager.Instance.result)
        {
            res.SetActive(false);
        }
        foreach (GameObject t in UIManager.Instance.tools)
        {
            t.SetActive(false);
        }
        UIManager.Instance.numOfBolts.gameObject.SetActive(false);
        UIManager.Instance.numOfGears.gameObject.SetActive(false);
        UIManager.Instance.numOfWheels.gameObject.SetActive(false);
    }
    public override void Exit()                                         //esco dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
