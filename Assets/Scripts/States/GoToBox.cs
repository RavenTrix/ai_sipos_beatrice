using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//lo stato GoToBox pu� essere richiamato in tre situazioni: dopo il Watch (subito dopo aver visto l'ordine), dopo lo Storage (quando la scatola � vuota) e dopo il Craft (se manca un materiale)
public class GoToBox : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToBox(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.GoToBox;                   //setto il nome dello stato attuale a GoToBox
        agent.speed = 10;                       //setto la velocit� del robot
        agent.isStopped = false;                //e mi assicuro che l'IA possa muoversi
    }
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //resetto l'animazione di rotazione e riattivo quella di camminata
        anim.SetBool("Walk_Anim", true);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }
    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        //primo giro alle scatole dei materiali (primo materiale da raccogliere)
        if (GameManager.instance.numOfMaterialsTaken == 0)
        {
            box = ClosestBox();                                                                             //la scatola di destinazione sar� quella pi� vicina all'IA
            agent.SetDestination(box.position);
            if (agent.transform.position == box.position)                                                   //se il robot si trova davanti alla scatola contenente il primo materiale
            {
                VisualEffects();                                                                            //si disattiva il VFX della ricarica partito nello stato Storage
                nextState = new GoToTable(agent, anim, box, storage, table, recharge, center, cBelt);       //passa allo stato successivo: andare al tavolo da lavoro
                stage = Event.Exit;                                                                         //ed esce dallo stato attuale
                return;
            }
        }
        //secondo giro alle scatole dei materiali (secondo materiale da raccogliere)
        else if (GameManager.instance.numOfMaterialsTaken != 0)
        {
            agent.SetDestination(GameManager.instance.currentToolsPoints[0].transform.position);            //l'ultima scatola rimasta nella lista di quelle utili per il progetto attuale diventa la destinazione dell'IA
            if (agent.transform.position == GameManager.instance.currentToolsPoints[0].transform.position)  //se l'IA raggiunge la scatola
            {
                VisualEffects();                                                                            //si disattiva il VFX della ricarica partito nello stato Storage
                nextState = new GoToTable(agent, anim, box, storage, table, recharge, center, cBelt);       //passa allo stato successivo: andare al tavolo da lavoro
                stage = Event.Exit;                                                                         //ed esce dallo stato attuale
                return;
            }
        }

        base.Update();
    }
    public void VisualEffects()                                         //creo un metodo che gestisca la chiusura di ogni VFX applicato alle scatole nello stato Storage
    {
        foreach (GameObject vfx in VFX.instance.boxRechargeVFX)
        {
            vfx.SetActive(false);
        }
    }
    public override void Exit()                                         //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
