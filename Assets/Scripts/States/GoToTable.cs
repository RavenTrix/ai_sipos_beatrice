using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//lo stato GoToTable pu� essere richiamato 2 volte: dopo aver preso il primo materiale dalla scatola e dopo aver preso il secondo materiale dall'altra scatola
public class GoToTable : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToTable(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.GoToTable;                         //setto il nome dello stato attuale a GoToTable
        agent.speed = 10;                               //setto la velocit� del robot
        agent.isStopped = false;                        //e mi assicuro che l'IA possa muoversi
    }
    public override void Enter()                        //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);               //disattivo l'animazione di rotazione e attivo quella di apertura e camminata
        anim.SetBool("Walk_Anim", true);
        anim.SetBool("Open_Anim", true);

        GameManager.instance.numOfMaterialsTaken++;     //aumento il conteggio di materiali raccolti

        base.Enter();
    }
    public override void Update()                                                               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        agent.SetDestination(table.position);                                                   //setto come destinazione del robot il tavolo da lavoro
        if (agent.transform.position == table.position)                                         //se il robot si trova davanti al tavolo
        {
            nextState = new Craft(agent, anim, box, storage, table, recharge, center, cBelt);   //passa allo stato successivo: la costruzione (o l'analisi del materiale mancante)
            stage = Event.Exit;                                                                 //ed esce dallo stato attuale
            return;
        }
        base.Update();
    }
    public override void Exit()         //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
