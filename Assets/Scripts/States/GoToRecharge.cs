using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//GoToRecharge viene chiamato solo quando il robot ha terminato un ordine se la sua energia si � esaurita (si esaurisce pi� o meno quando � calato il sole, quindi circa ogni 3 ordini)
public class GoToRecharge : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToRecharge(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        agent.speed = 6;                       //setto la velocit� del robot
        name = State.GoToRecharge;             //setto il nome dello stato attuale a GoToRecharge
        agent.isStopped = false;               //e mi assicuro che l'IA possa muoversi
    }
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //disattivo l'animazione della rotazione e attivo le altre
        anim.SetBool("Walk_Anim", true);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }
    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        if (!IsCharged())                                                                               //se � scarico
            agent.SetDestination(recharge.position);                                                    //setto come destinazione la piattaforma di ricarica
        
        if (agent.transform.position == recharge.position)                                              //se il robot si trova davanti alla piattaforma di ricarica
        {
            nextState = new Recharge(agent, anim, box, storage, table, recharge, center, cBelt);        //passa allo stato successivo: quello di ricarica
            stage = Event.Exit;                                                                         //esce dallo stato attuale
            return;
        }
        base.Update();
    }
    public override void Exit()                 //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
