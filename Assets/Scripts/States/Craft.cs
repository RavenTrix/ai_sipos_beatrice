using System.Collections;
using System.Collections.Generic;
using Unity.Profiling;
using UnityEngine;
using UnityEngine.AI;

//lo stato Craft viene richiamato sia quando ha un solo materiale, sia quando li ha tutti e due (da un lato controlla l'esistenza del materiale occorrente nella scatola, dall'altro ripara l'oggetto)
public class Craft : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public Craft(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.Craft;                     //setto il nome dello stato attuale a Craft
    }
    public float Timer;                         //creo un timer che permetta al robot di riparare l'oggetto prima di partire per il nastro trasportatore
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //disattivo tutte le animazioni tranne quella di apertura
        anim.SetBool("Walk_Anim", false);
        anim.SetBool("Open_Anim", true);

        ActivateCraftingObjects();              //attivo l'oggetto da riparare sul tavolo da lavoro
        RemovePreviousMaterial();               //e rimuovo dalla lista dei materiali necessari quello appena preso

        agent.isStopped = true;                 //mi assicuro che il robot si fermi
        base.Enter();
    }
    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        if ((GameManager.instance.currentToolsPoints[0].GetComponent<Box>().isEmpty && GameManager.instance.numOfMaterialsTaken == 1))  //se la seconda scatola (quella rimanente nella lista) � vuota e ho gi� preso un materiale
        {
            nextState = new GoToStorage(agent, anim, box, storage, table, recharge, center, cBelt);                                     //passo allo stato successivo: andare al magazzino dei rifornimenti
            stage = Event.Exit;                                                                                                         //ed esco dallo stato attuale
            return;
        }
        if (GameManager.instance.numOfMaterialsTaken == 1)                                                                              //se il robot ha preso un materiale
        {
            if (agent.transform.position == table.position)                                                                             //e si trova davanti al tavolo
            {
                nextState = new GoToBox(agent, anim, box, storage, table, recharge, center, cBelt);                                     //passo allo stato successivo: andare all'altra scatola
                stage = Event.Exit;                                                                                                     //ed esco dallo stato attuale
                return;
            }
        }
        else                                                                                                                            //se il robot ha preso tutti e due i materiali
        {
            VFX.instance.dustFX.SetActive(true);                                                                                        //attivo il VFX del polverone da lavoro
            Timer += Time.deltaTime;                                                                                                    //e parte il timer di 5 secondi
            if (Timer > 5)
            {
                DeactivateVFXandProps();                                                                                                //dopodich� si disattiva sia il polverone sia l'oggetto da riparare
                nextState = new GoToConveyerBelt(agent, anim, box, storage, table, recharge, center, cBelt);                            //e passo allo stato successivo: andare al nastro trasportatore
                stage = Event.Exit;                                                                                                     //poi esco dallo stato attuale
                return;
            }
            GameManager.instance.numOfMaterialsTaken = 0;                                                                               //resetto il numero di materiali raccolti per il round successivo
        }
        base.Update();
    }
    public void ActivateCraftingObjects()                                       //in base al progetto da creare comparso sullo schermo, si attiva l'oggetto corrispondente sul tavolo
    {
        if (UIManager.Instance.result[0].activeSelf)
            ToolsManager.Instance.bWBCraft.SetActive(true);
        if (UIManager.Instance.result[1].activeSelf)
            ToolsManager.Instance.bikeCraft.SetActive(true);
        if (UIManager.Instance.result[2].activeSelf)
            ToolsManager.Instance.sWBCraft.SetActive(true);
    }
    public void RemovePreviousMaterial()                                        //se ho gi� raccolto il primo materiale, lo rimuovo dalla lista dei materiali utili per il progetto
    {
        if (GameManager.instance.numOfMaterialsTaken == 1)
        {
            if (GameManager.instance.FirstBoxIsZero)                            //se il primo materiale si trovava nella scatola con indice zero nella lista, rimuove l'elemento zero dalla lista
                GameManager.instance.currentToolsPoints.RemoveAt(0);
            else
                GameManager.instance.currentToolsPoints.RemoveAt(1);            //in caso contrario, rimuove l'elemento uno dalla lista
        }
    }
    public void DeactivateVFXandProps()                                         //disattivo il VFX del polverone e ogni oggetto attivato sul tavolo
    {
        VFX.instance.dustFX.SetActive(false);
        ToolsManager.Instance.bWBCraft.SetActive(false);
        ToolsManager.Instance.bikeCraft.SetActive(false);
        ToolsManager.Instance.sWBCraft.SetActive(false);
    }
    public override void Exit()                                                 //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
