using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Il primo stato di gioco � GoToCenter, il robot (in qualunque stato si trovi), andr� verso il centro della scena
public class GoToCenter : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public GoToCenter(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
         : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        agent.speed = 10;                       //setto la velocit� del robot
        name = State.GoToCenter;                //setto il nome dello stato attuale a GoToCenter
        agent.isStopped = false;                //e mi assicuro che l'IA possa muoversi
    }

    public float delay;                         //creo un timer per creare un delay iniziale

    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //attivo le animazioni di apertura e camminata del robot e disattivo quella di rotazione
        anim.SetBool("Walk_Anim", true);
        anim.SetBool("Open_Anim", true);
        base.Enter();
    }

    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        delay += Time.deltaTime;                //faccio partire il timer che crea un delay iniziale


        //se sono passati 3.5 secondi, il robot non si trova in centro e non ha ancora mai fatto un ordine    o    ha gi� fatto almeno un ordine e non � passato neanche un secondo
        if ((delay > 3.5f && agent.transform.position != center.position && !GameManager.instance.orderDone) || GameManager.instance.orderDone && delay >= 0)
        {
            GameManager.instance.orderDone = true;                                                          //� cominciato il primo ordine, perci� le prossime volte non ci sar� delay
            agent.SetDestination(center.position);                                                          //la destinazione del robot sar� il centro della scena
        }

        //se il robot si trova in centro ed � carico
        if (agent.transform.position == center.position && IsCharged())
        {
            anim.SetBool("Walk_Anim", false);                                                               //la sua animazione di camminata si ferma
            nextState = new Watch(agent, anim, box, storage, table, recharge, center, cBelt);               //passa al prossimo stato: l'osservazione degli ordini
            stage = Event.Exit;                                                                             //ed esce ufficialmente da questo stato
            return;
        }

        //se il robot non � carico (sia che si trovi al centro o meno
        if (!IsCharged())
        {
            nextState = new GoToRecharge(agent, anim, box, storage, table, recharge, center, cBelt);        //passa al prossimo stato: la corsa per la ricarica
            stage = Event.Exit;                                                                             //ed esce ufficialmente da questo stato
            return;
        }
        base.Update();
    }

    public override void Exit()         //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
