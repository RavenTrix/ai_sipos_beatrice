using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Recharge viene chiamato solo quando il robot ha raggiunto la piattaforma di ricarica energetica
public class Recharge : AIState
{
    //riprendo la firma del costruttore che prende come parametri:
    //l'intelligenza artificiale, l'animazione, la scatola di materiali, i magazzini, il tavolo da lavoro, la base di ricarica, il centro della scena e il nastro trasportatore
    public Recharge(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
        : base(_agent, _anim, _box, _storage, _table, _recharge, _center, _cBelt)
    {
        name = State.Recharge;                  //setto il nome dello stato attuale a Recharge
    }
    public override void Enter()                //richiamo lo status Enter che funge da equivalente dello Start
    {
        anim.SetBool("Roll_Anim", false);       //disattivo tutte le animazioni (cos� il robot si chiude)
        anim.SetBool("Walk_Anim", false);
        anim.SetBool("Open_Anim", false);
        agent.isStopped = true;                 //mi assicuro che il robot si fermi
        base.Enter();
    }
    public override void Update()               //richiamo lo status Update che svolge la stessa funzione dell'Update che eredita da Monobehaviour
    {
        if (EnergyManager.Instance.energy < EnergyManager.Instance.maxEnergy)                           //se l'energia del robot � minore dell'energia massima (e si trova sulla piattaforma)
            EnergyManager.Instance.energy += 4.8f * Time.deltaTime;                                     //l'energia risale di 4.8 al secondo
        
        else
        {
            nextState = new GoToCenter(agent, anim, box, storage, table, recharge, center, cBelt);      //se l'energia � di nuovo al massimo, passa allo stato successivo: torna al centro
            stage = Event.Exit;                                                                         //ed esce dallo stato attuale
            return;
        }
        base.Update();
    }
    public override void Exit()                 //esce dallo stato attuale, chiudendolo
    {
        base.Exit();
    }
}
