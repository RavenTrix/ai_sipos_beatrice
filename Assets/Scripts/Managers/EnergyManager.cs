using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : MonoBehaviour
{
    public static EnergyManager Instance;                       //creo un'istanza statica della classe
    public float energy;                                        //creo un contatore di energia
    public float maxEnergy;                                     //e un massimo di energia
    AIController controller;                                    //richiamo l'AIController che mi servir� per controllarne lo stato
    private void Awake()
    {
        if (Instance != null)                                   //se esiste gi� un'istanza di questa classe, distruggo questa versione
            Destroy(gameObject);
        else
            Instance = this;                                    //altrimenti la classe istanziata � questa
    }
    void Start()
    {
        controller = FindObjectOfType<AIController>();
        energy = maxEnergy;                                     //setto l'energia al massimo
    }

    void Update()
    {
        if (energy >= 0 && controller.currentState.name.ToString() != "Recharge")           //se l'energia � maggiore di zero e lo stato di gioco non � Recharge
            energy -= Time.deltaTime;                                                       //l'energia scende col tempo
    }
}
