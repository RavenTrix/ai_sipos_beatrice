using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance { get => instance; private set => instance = value; }      //creo un'istanza statica della classe

    public Image energyBar;                                                                     //creo un'immagine che funga da barra dell'energia
    public GameObject[] result;                                                                 //un array di immagini (gameobject) di risultati (carriola, bici e carrello)
    public GameObject[] tools;                                                                  //un array di immagini (gameobject) di materiali/attrezzi
    public GameObject[] materialsInBox;                                                         //un array di materiali nelle scatole
    [HideInInspector] public int numOfResult;                                                   //un indicatore del risultato
    public TextMeshProUGUI numOfGears, numOfWheels, numOfBolts;                                 //il testo che compare sopra ai materiali da prendere
    public TextMeshProUGUI numOfGearsInBox, numOfWheelsInBox, numOfBoltsInBox;                  //il testo che compare sulle scatole dei materiali
    public TextMeshProUGUI accelerate;                                                          //il testo sul pulsante di accelerazione del tempo
    bool activated;                                                                             //una booleana che indichi se il pulsante di accelerazione � gi� stato attivato o meno
    private void Awake()
    {
        if (instance != null)                                   //se esiste gi� un'istanza di questa classe distrugge questa
            Destroy(gameObject);
        else
            instance = this;                                    //senn� la classe istanziata � questa
    }
    private void Update()
    {
        UIUpdate();                                             //aggiorno la UI
    }
    public void RandomChoice()
    {
        for (int i = 0; i < numOfResult; i++)                   //per ogni risultato scelto randomicamente si attiva la UI corrispondente
        {
            if (i == numOfResult)
                result[i].SetActive(true);
            else
                result[i].SetActive(false);
        }
        result[numOfResult].SetActive(true);
        if (numOfResult == 0)                                   //se il risultato � la carriola
        {
            numOfGears.gameObject.SetActive(false);             //si disattiva il contatore degli ingranaggi
            numOfWheels.gameObject.SetActive(true);             //si attiva il contatore delle ruote
            numOfBolts.gameObject.SetActive(true);              //si attiva il contatore dei bulloni
            numOfWheels.text = "1";                             //il numero che compare sopra alle ruote � 1
            numOfBolts.text = "1";                              //il numero che compare sopra ai bulloni � 1
            tools[0].SetActive(true);                           //si attiva la UI dei bulloni
            tools[1].SetActive(true);                           //si attiva la UI delle ruote
            tools[2].SetActive(false);                          //si disattiva la UI degli ingranaggi
        }
        if (numOfResult == 1)                                   //se il risultato � la bici
        {
            numOfBolts.gameObject.SetActive(false);             //si disattiva il contatore dei bulloni
            numOfGears.gameObject.SetActive(true);              //si attiva il contatore degli ingranaggi
            numOfWheels.gameObject.SetActive(true);             //si attiva il contatore delle ruote
            numOfGears.text = "1";                              //il numero che compare sopra agli ingranaggi � 1
            numOfWheels.text = "2";                             //il numero che compare sopra alle ruote � 2
            tools[0].SetActive(false);                          //si disattiva la UI dei bulloni
            tools[1].SetActive(true);                           //si attiva la UI delle ruote
            tools[2].SetActive(true);                           //si attiva la UI degli ingranaggi
        }
        if (numOfResult == 2)                                   //se il risultato � il carrello
        {
            numOfGears.gameObject.SetActive(false);             //si disattiva il contatore degli ingranaggi
            numOfWheels.gameObject.SetActive(true);             //si attiva il contatore delle ruote
            numOfBolts.gameObject.SetActive(true);              //si attiva il contatore dei bulloni
            numOfWheels.text = "2";                             //il numero che compare sopra alle ruote � 2
            numOfBolts.text = "2";                              //il numero che compare sopra ai bulloni � 2
            tools[0].SetActive(true);                           //si attiva la UI dei bulloni
            tools[1].SetActive(true);                           //si attiva la UI delle ruote
            tools[2].SetActive(false);                          //si disattiva la UI degli ingranaggi
        }
    }
    public void Esc()                                           //pulsante Quit
    {
        Application.Quit();
    }
    public void Accelerate()                                    //pulsante accelerazione del tempo
    {
        if (!activated)                                         //se non l'ho mai cliccato
        {
            accelerate.text = ">";                              //accelera e si attiva
            Time.timeScale = 4f;
            activated = true;
        }
        else
        {
            accelerate.text = ">>";                             //altrimenti rallenta e si disattiva
            Time.timeScale = 1f;
            activated = false;
        }
    }
    public void UIUpdate()                                                                              //aggiorna la UI
    {
        energyBar.fillAmount = EnergyManager.Instance.energy / EnergyManager.Instance.maxEnergy;        //aggiorna la barra dell'energia
        numOfResult = UnityEngine.Random.Range(0, result.Length);                                       //sceglie randomicamente quale risultato deve uscire
        numOfGearsInBox.text = materialsInBox[0].GetComponent<Box>().counter.ToString() + "/5";         //aggiorna il contatore della scatola di ingranaggi
        numOfWheelsInBox.text = materialsInBox[1].GetComponent<Box>().counter.ToString() + "/5";        //aggiorna il contatore della scatola di ruote
        numOfBoltsInBox.text = materialsInBox[2].GetComponent<Box>().counter.ToString() + "/5";         //aggiorna il contatore della scatola di bulloni
    }
}
