using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AIController : MonoBehaviour
{
    [SerializeField] Transform Box;                 //la scatola che verr� scelta
    [SerializeField] Transform[] Storage;           //i magazzini dei materiali
    [SerializeField] Transform Table;               //il tavolo da lavoro
    [SerializeField] Transform Recharge;            //la piattaforma di ricarica
    [SerializeField] Transform Center;              //il centro della scena
    [SerializeField] Transform CBelt;               //il nastro trasportatore

    NavMeshAgent agent;                             //l'IA (robot)
    [SerializeField] Animator anim;                 //l'animazione
    [HideInInspector] public AIState currentState;  //lo stato attuale

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        currentState = new GoToCenter(agent, anim, Box, Storage, Table, Recharge, Center, CBelt);   //il primo stato � GoToCenter (il robot va al centro della scena)
    }
    private void Update()
    {
        currentState = currentState.Process();      //durante l'update avviene il passaggio tra uno status all'altro (Enter, Update ed Exit) per poi passare all'azione successiva
    }

    private void OnTriggerEnter(Collider other)
    {
        Box box = other.GetComponent<Box>();                                        //creo una variabile di tipo box che contenga la scatola verso la quale va l'IA
        if (other.CompareTag("Box") && currentState.name.ToString() == "GoToBox")   //se il collider attraversato ha il tag Box e lo stato di gioco � GoToBox
        {
            // 0) per la CARRIOLA servono       1 RUOTA         e       1 BULLONE
            // 1) per le BICI servono           2 RUOTE         e       1 INGRANAGGIO
            // 2) per il CARRELLO servono       2 RUOTE         e       2 BULLONI

            //se la scatola contiene ingranaggi
            //o contiene ruote e il risultato da ottenere � una carriola
            //o contiene bulloni e il risultato da ottenere � una carriola
            if (box.type == TypeOfTool.Gears || (box.type == TypeOfTool.Wheels && UIManager.Instance.result[0].activeSelf) || ((box.type == TypeOfTool.Bolts && UIManager.Instance.result[0].activeSelf)))
                box.counter--;      //il conteggio dei materiali contenuti nella scatola scende di uno
            else
                box.counter-=2;     //negli altri casi, scende di due (come nella bici (ruote) e nel carrello (ruote e bulloni))
        }
    }
}

