using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }                //creo un'istanza statica della classe
    [HideInInspector] public bool orderDone;                                //creo una booleana che indichi se � gi� stato fatto almeno un ordine o no
    [HideInInspector] public int numOfMaterialsTaken;                       //creo un contatore di materiali raccolti
    public bool FirstBoxIsZero;                                             //una booleana che indichi se la prima scatola da raggiungere � la prima della lista o meno
    public GameObject sunLight;                                             //un gameobject che contenga la luce del sole
    public List<GameObject> toolsPoints = new List<GameObject>();           //una lista di punti (scatole dei materiali/attrezzi)
    public List<GameObject> currentToolsPoints = new List<GameObject>();    //una lista dei punti da raggiungere (scatole dei materiali/attrezzi attuali)
    AIController controller;                                                //creo una reference da AIController che mi servir� per controllare lo status di gioco
    private void Awake()
    {
        if (instance != null)                                               //se esiste gi� un'istanza di questa classe distrugge questa
            Destroy(gameObject);
        else
            instance = this;                                                //senn� la classe istanziata � questa
    }
    private void Start()
    {
        controller = FindObjectOfType<AIController>();
    }
    void Update()
    {
        if (controller.currentState.name.ToString() == "Recharge")                              //se lo status � di ricarica
            sunLight.transform.Rotate(12.15f * Vector3.left * Time.deltaTime, Space.World);     //la luce del sole ruota pi� velocemente
        else
            sunLight.transform.Rotate(0.39f * Vector3.left * Time.deltaTime, Space.World);      //senn� ruota pi� lentamente
    }
    public void AssignToolsToPoints()                                                           //assegno alla lista i punti relativi alle scatole da raggiungere attualmente
    {
        // 0) per la CARRIOLA servono       1 RUOTA         e       1 BULLONE
        // 1) per le BICI servono           2 RUOTE         e       1 INGRANAGGIO
        // 2) per il CARRELLO servono       2 RUOTE         e       2 BULLONI

        // 0) BULLONI
        // 1) RUOTE
        // 2) INGRANAGGi

        //se il risultato � una carriola o un carrello e la lista attuale non contiene ancora la scatola dei bulloni e delle ruote
        if ((UIManager.Instance.numOfResult == 0 || UIManager.Instance.numOfResult == 2) && !currentToolsPoints.Contains(toolsPoints[0]) && !currentToolsPoints.Contains(toolsPoints[1]))
        {
            currentToolsPoints.Add(toolsPoints[0].gameObject); //aggiunge la scatola dei bulloni
            currentToolsPoints.Add(toolsPoints[1].gameObject); //e la scatola delle ruote
        }

        //se il risultato � la bici e la lista attuale non contiene ancora la scatola delle ruote o degli ingranaggi
        if (UIManager.Instance.numOfResult == 1 && !currentToolsPoints.Contains(toolsPoints[1]) && !currentToolsPoints.Contains(toolsPoints[2]))
        {
            currentToolsPoints.Add(toolsPoints[2]); //aggiunge la scatola degli ingranaggi
            currentToolsPoints.Add(toolsPoints[1]); //e la scatola delle ruote
        }
    }
}
