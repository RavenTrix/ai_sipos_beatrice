using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public enum State
{
    Watch,                  //visualizzazione progetto
    GoToBox,                //raccolta dei materiali nelle scatole
    GoToTable,              //trasporto materiali verso il tavolo
    GoToStorage,            //rifornimento nei magazzini
    GoToConveyerBelt,       //consegna del risultato finale
    GoToRecharge,           //raggiungimento della postazione di ricarica
    Recharge,               //ricarica
    Craft,                  //assemblaggio
    GoToCenter              //ritorno postazione centrale
}
public class AIState
{
    public enum Event       //creo un enum di status (Enter = Start, Update, Exit)
    {
        Enter,
        Update,
        Exit
    }
    public State name;
    public Event stage;

    protected NavMeshAgent agent;       //IA
    protected Animator anim;            //animazione
    protected Transform box;            //scatola scelta
    protected Transform[] storage;      //magazzini
    protected Transform table;          //tavolo
    protected Transform recharge;       //ricarica
    protected Transform center;         //postazione centrale
    protected Transform cBelt;          //nastro trasportatore
    protected AIState nextState;        //stato successivo

    //creo un costruttore che richiamer� in ogni stato
    public AIState(NavMeshAgent _agent, Animator _anim, Transform _box, Transform[] _storage, Transform _table, Transform _recharge, Transform _center, Transform _cBelt)
    {
        stage = Event.Enter;
        agent = _agent;
        anim = _anim;
        box = _box;
        storage = _storage;
        table = _table;
        recharge = _recharge;
        center = _center;
        cBelt = _cBelt;
    }
    public virtual void Enter() { stage = Event.Update; }       //in enter parte l'Update
    public virtual void Update() { stage = Event.Update; }      //in update, continua l'Update finch� non viene chiamato l'Exit
    public virtual void Exit() { stage = Event.Exit; }          //in exit, esce dallo stato e passa al successivo

    public AIState Process()                                    //in process si passa da Enter ad Update e ad Exit fino allo stato successivo
    {
        if (stage == Event.Enter) Enter();
        if (stage == Event.Update) Update();
        if (stage == Event.Exit)
        {
            Exit();
            return nextState;
        }
        return this;
    }
    public Transform ClosestBox()   //restituisce la scatola pi� vicina al robot
    {
        Vector3 box0 = GameManager.instance.currentToolsPoints[0].transform.position - agent.transform.position;    //calcolo la distanza tra la prima scatola della lista e l'IA
        Vector3 box1 = GameManager.instance.currentToolsPoints[1].transform.position - agent.transform.position;    //calcolo la distanza tra la seconda scatola della lista e l'IA
        if (box0.magnitude < box1.magnitude)                                                                        //se la distanza dalla prima scatola � minore rispetto a quella dalla seconda
        {
            GameManager.instance.FirstBoxIsZero = true;                                                             //la prima scatola da raggiungere � la prima della lista
            return GameManager.instance.currentToolsPoints[0].transform;                                            //perci� la restituisco
        }
        else
        {
            GameManager.instance.FirstBoxIsZero = false;                                                            //senn� la prima scatola da raggiungere � la seconda della lista
            return GameManager.instance.currentToolsPoints[1].transform;                                            //perci� la restituisco
        }
    }
    public bool IsCharged()                                                                                         //creo una booleana di ricarica
    {
        if (EnergyManager.Instance.energy <= 0)                                                                     //se l'energia � minore di zero � falsa
            return false;
        else
            return true;                                                                                            //senn� il robot � carico
    }
}
