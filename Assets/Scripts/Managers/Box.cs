using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//creo un enum di materiali/attrezzi utilizzati per riparare gli oggetti
public enum TypeOfTool
{
    Bolts,
    Wheels,
    Gears
}
public class Box : MonoBehaviour
{
    public bool isEmpty;                    //creo una booleana che indica se la scatola � piena o vuota
    public int counter;                     //un contatore di materiali all'interno della scatola
    public int maxTools;                    //un massimo di materiali
    public TypeOfTool type;                 //il tipo di materiali
    public GameObject materialsInTheBox;    //e gli oggetti effettivi che verranno attivati e disattivati ogni volta che la scatola si riempie o si svuota
    private void Start()
    {
        counter = maxTools;                 //setto la quantit� di materiali della scatola al massimo
    }
    private void Update()
    {
        // 0) per la CARRIOLA servono       1 RUOTA         e       1 BULLONE
        // 1) per le BICI servono           2 RUOTE         e       1 INGRANAGGIO
        // 2) per il CARRELLO servono       2 RUOTE         e       2 BULLONI

        //se non ci sono materiali
        //o ce n'� uno solo (con risultato bici) e si tratta di una ruota
        //o ce n'� uno solo (con risultato carrello) e si tratta di una ruota o di un bullone
        if (counter <= 0 || (counter == 1 && (UIManager.Instance.result[1].activeSelf && type == TypeOfTool.Wheels)) || counter == 1 && UIManager.Instance.result[2].activeSelf && (type == TypeOfTool.Wheels || type == TypeOfTool.Bolts))
        {
            isEmpty = true;                         //la scatola � vuota
            materialsInTheBox.SetActive(false);     //e si disattivano i materiali al suo interno (perci� in alcuni casi anche se c'� ancora una ruota od un bullone, le loro scatole si svuotano perch� ne servono almeno 2 per riparare gli oggetti)
        }
        else
            isEmpty = false;                        //in caso contrario � ancora piena
    }
}
