using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX : MonoBehaviour
{
    public static VFX instance { get; private set; }        //creo un'istanza della classe
    public GameObject[] boxRechargeVFX;                     //creo un array di VFX di ricarica delle scatole
    public GameObject dustFX;                               //un gameobject che contiene il VFX del polverone
    public GameObject flashlightFX;                         //un gameobject che contiene il VFX dell'allarme Recharge
    private void Awake()
    {
        if (instance != null)                               //se esiste gi� un'istanza di questa classe distruggo questa versione
            Destroy(gameObject);
        else                                                //senn� la classe istanziata � quella attuale
            instance = this;
    }
    private void Update()
    {
        if (EnergyManager.Instance.energy <= 0)             //se l'energia � minore o uguale a 0 si attiva l'allarme senn� si disattiva
        {
            flashlightFX.SetActive(true);
        }
        else
            flashlightFX.SetActive(false);
    }
}
