using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//questo script si trova all'interno degli oggetti che compaiono sopra al nastro trasportatore
public class ObjectDespawn : MonoBehaviour
{
    private Vector3 startPos;                       //creo una posizione iniziale
    void Start()
    {
        startPos = transform.position;              //la setto
    }

    private void OnTriggerEnter(Collider other)     //e quando l'oggetto entra in contatto con l'area di despawn
    {
        if(other.CompareTag("Despawn"))
        {
            gameObject.SetActive(false);            //l'oggetto si disattiva
            transform.position = startPos;          //e spawna alla sua posizione iniziale
        }
    }
}
