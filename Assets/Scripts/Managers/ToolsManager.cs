using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolsManager : MonoBehaviour
{
    public static ToolsManager Instance { get; private set; }           //creo un'istanza statica della classe
    public GameObject bikeCraft, sWBCraft, bWBCraft;                    //questi sono gli oggetti che compariranno sul tavolo da lavoro
    public GameObject bike, smallWheelBarrow, bigWheelBarrow;           //questi sono gli oggetti che compariranno sul nastro trasportatore
    private void Awake()
    {
        if (Instance != null)                                           //se esiste gi� un'istanza di questa classe distrugge questa
            Destroy(gameObject);
        else
            Instance = this;                                            //altrimenti la classe istanziata � questa
    }

    //se uno degli oggetti sul nastro trasportatore � attivo, si sposta verso il suo forward
    private void Update()
    {
        if (bike.activeSelf)                    
            bike.transform.Translate(2 * Vector3.forward * Time.deltaTime);
        if (smallWheelBarrow.activeSelf)
            smallWheelBarrow.transform.Translate(2 * Vector3.forward * Time.deltaTime);
        if (bigWheelBarrow.activeSelf)
            bigWheelBarrow.transform.Translate(2 * Vector3.forward * Time.deltaTime);
    }
}
